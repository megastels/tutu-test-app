import Select, { SelectProps, SelectDispatchProps } from "../components/Select";
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux'
import { changeCurrency, ChangeCurrencyAction } from "../actions";
import { CurrencyItem } from "../interfaces/CurrencyItem";
import { State } from "../reducers";
import { Dispatch } from "react";


const mapStateToProps: MapStateToProps<SelectProps<CurrencyItem>, {}, State> = (state: State) => ({
	selected: state.currency.selectedCurrency,
	values: state.currency.list,
	disabled: state.currency.pending || state.currency.hasErrors
});


const mapDispatchToProps: MapDispatchToProps<SelectDispatchProps<CurrencyItem>, {}> = (dispatch: Dispatch<ChangeCurrencyAction>) => {
	return {
		onChange: (item: CurrencyItem) => dispatch(changeCurrency(item))
	}
};


export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Select);