export const SortAsc = <T>(key: keyof T) => {
	return (a: T, b: T) => a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
}

export const SortDesc = <T>(key: keyof T) => {
	return (a: T, b: T) => a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0;
}