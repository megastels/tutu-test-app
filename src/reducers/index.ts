import { combineReducers } from "redux";
import currency, { CurrencyState } from "./currency";
import data, { DataState } from "./data";

export interface State {
	currency: CurrencyState,
	data: DataState
}
export default combineReducers<State>({
	currency,
	data
});