import { CHANGE_CURRENCY, UPDATE_CURRENCY_LIST, CurrencyActions, SET_PENDING_STATUS, SET_ERROR, CLEAR_ERROR } from "../actions";
import { CurrencyItem } from "../interfaces/CurrencyItem";

export interface CurrencyState {
	list: CurrencyItem[],
	selectedCurrency: CurrencyItem,
	pending: boolean,
	hasErrors: boolean,
	errorText: string | null
}


const currencyList: CurrencyItem[] = [
	{ name: "Рубли", value: "RUB", nominal: 1 }
];

const defaultState: CurrencyState = {
	list: currencyList,
	selectedCurrency: currencyList[0],
	pending: false,
	hasErrors: false,
	errorText: null
}


const currency = (state = defaultState, action: CurrencyActions): CurrencyState => {
	switch (action.type) {
		case CHANGE_CURRENCY:
			return {
				...state,
				selectedCurrency: action.currency
			};
		case UPDATE_CURRENCY_LIST:

			let list = [
				action.list["USD"],
				action.list["EUR"]
			].map(item => {

				let currencyItem: CurrencyItem = {
					name: item.Name,
					value: item.CharCode,
					nominal: item.Value
				};
				return currencyItem;
			});
			return {
				...state,
				list: currencyList.concat(list)
			}

		case SET_PENDING_STATUS:
			return {
				...state,
				pending: action.isPending
			}
		case SET_ERROR:
			return {
				...state,
				hasErrors: true,
				errorText: action.text
			}
		case CLEAR_ERROR:
			return {
				...state,
				hasErrors: false,
				errorText: null
			}
		default:
			return state;
	}
}

export default currency;