import { SortType, SORT, DataActions } from "../actions";
import { ArrivalItem } from "../interfaces/ArrivalItem";
import { SortAsc, SortDesc } from "../utils";

const defaultData = [
	{
		id: 1,
		departure: 'Москва',
		arrival: 'Тула',
		departureDate: '2019-07-10T08:00:00.000Z',
		arrivalDate: '2019-07-10T11:00:00.000Z',
		price: 500 // стоимость в рублях
	},
	{
		id: 2,
		departure: 'Москва',
		arrival: 'Тула',
		departureDate: '2019-07-10T10:00:00.000Z',
		arrivalDate: '2019-07-10T13:00:00.000Z',
		price: 550 // стоимость в рублях
	},
	{
		id: 3,
		departure: 'Москва',
		arrival: 'Тула',
		departureDate: '2019-07-10T14:00:00.000Z',
		arrivalDate: '2019-07-10T16:30:00.000Z',
		price: 550 // стоимость в рублях
	},
	{
		id: 4,
		departure: 'Москва',
		arrival: 'Тула',
		departureDate: '2019-07-10T10:00:00.000Z',
		arrivalDate: '2019-07-10T12:30:00.000Z',
		price: 510 // стоимость в рублях
	},
	{
		id: 5,
		departure: 'Москва',
		arrival: 'Тула',
		departureDate: '2019-07-10T17:00:00.000Z',
		arrivalDate: '2019-07-10T20:00:00.000Z',
		price: 500 // стоимость в рублях
	}
];


export interface DataState {
	list: ArrivalItem[],
	sortKey: keyof ArrivalItem,
	sortType: SortType,
}


const defaultState: DataState = {
	list: defaultData.map((item, index) => {
		return {
			id: item.id,
			index,
			arrival: item.arrival,
			departure: item.departure,
			arrivalDate: new Date(item.arrivalDate),
			departureDate: new Date(item.departureDate),
			price: item.price
		}
	}),
	sortKey: "index",
	sortType: SortType.NO
};


const data = (state = defaultState, action: DataActions): DataState => {
	switch (action.type) {
		case SORT:
			let newList = state.list.slice(0);
			switch (action.sortType) {
				case SortType.ASC:
					newList.sort(SortAsc(action.sortField));
					break;
				case SortType.DESC:
					newList.sort(SortDesc(action.sortField));
					break;
				case SortType.NO:
					newList.sort(SortAsc("index"));
					break;
			}

			return {
				list: newList,
				sortKey: action.sortField,
				sortType: action.sortType
			}
		default:
			return state;
	}
}

export default data;