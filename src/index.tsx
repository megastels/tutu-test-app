import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import * as serviceWorker from './serviceWorker';
import rootReducer, { State } from "./reducers";
import { createStore, applyMiddleware, AnyAction } from 'redux';
import { Provider } from "react-redux";
import { uploadCurreny } from './pseudo-actions';

const store = createStore(
	rootReducer,
	applyMiddleware(thunk as ThunkMiddleware<State, AnyAction>)
);
store.dispatch(uploadCurreny())

/* 
И вот тут я прямо почувствовал, что редакс разрабатывался эксклюзивно под JS. Не для TS. 
Кажется, что TypeScript только мешает.
В зависимости от того, какой был применен middleware к store,
в контейнер компонента  иожет прийти абсолютно любой Dispatch со своей сигнатурой.

тот же пример с Thunk.
Его middleware может вернуть реззультат работы функции.
в то время как обычный dispatch может вернуть только простой объект.
TS не будет ругаться если thunk удалить, а компоненты использующие его оставить.

И я пока не вижу способа, как контролировать это.

Тут либо вообще нельзя использовать thunk, чтобы иметь контроль за типами, 
либо надо как-то обходить эту проблему.
*/

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
