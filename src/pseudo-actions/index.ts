import { CurrencyResponse } from "../interfaces/CurrencyResponse";
import { setPendingStatus, updateCurrencyList, setError } from "../actions";
import { PseudoAction } from "../interfaces/PseudoAction";

export const uploadCurreny = (): PseudoAction<Promise<void>> => {
	return async (dispatch) => {

		dispatch(setPendingStatus(true));
		try {
			let res = await fetch("https://www.cbr-xml-daily.ru/daily_json.js");
			let data: CurrencyResponse = await res.json();
			dispatch(updateCurrencyList(data.Valute));
		}
		catch (e) {
			dispatch(setError("Не удалось загрузить список валют"));
			throw e;
		}
		finally {
			dispatch(setPendingStatus(false));
		}

	};
};
