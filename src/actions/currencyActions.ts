import { CurrencyItem } from "../interfaces/CurrencyItem";
import { ValuteList } from "../interfaces/CurrencyResponse";
import { Action } from "redux";

/*
 А почему везде, во всех примерах, используют именно строковые константы? 
 Можно же получить 2 экшена, с одинаковыми типами и все поломается...
 Symbol?
 */
export const CHANGE_CURRENCY = "CHANGE_CURRENCY";
export const UPDATE_CURRENCY_LIST = "UPDATE_CURRENCY_LIST";
export const SET_PENDING_STATUS = "SET_PENDING_STATUS";
export const SET_ERROR = "SET_ERROR";
export const CLEAR_ERROR = "CLEAR_ERROR";



export interface ChangeCurrencyAction extends Action<typeof CHANGE_CURRENCY> {
	currency: CurrencyItem
}

export interface UpdateCurrencyListAction extends Action<typeof UPDATE_CURRENCY_LIST> {
	list: ValuteList
}

export interface SetPendingStatusAction extends Action<typeof SET_PENDING_STATUS> {
	isPending: boolean;
}

export interface SetErrorAction extends Action<typeof SET_ERROR> {
	text: string;
}

export type ClearErrorAction = Action<typeof CLEAR_ERROR>;

export type CurrencyActions = ChangeCurrencyAction | UpdateCurrencyListAction | SetPendingStatusAction | SetErrorAction | ClearErrorAction;



export const changeCurrency = (currency: CurrencyItem): ChangeCurrencyAction => ({
	type: CHANGE_CURRENCY,
	currency
});

export const updateCurrencyList = (list: ValuteList): UpdateCurrencyListAction => ({
	type: UPDATE_CURRENCY_LIST,
	list: list
});

export const setPendingStatus = (isPending: boolean): SetPendingStatusAction => ({
	type: SET_PENDING_STATUS,
	isPending
});

export const setError = (text: string): SetErrorAction => ({
	type: SET_ERROR,
	text
});

export const clearError = (): ClearErrorAction => ({
	type: CLEAR_ERROR
});


