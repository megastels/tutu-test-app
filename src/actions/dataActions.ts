import { ArrivalItem } from "../interfaces/ArrivalItem";
import { Action } from "redux";

export enum SortType {
	NO,
	ASC,
	DESC
}


export const SORT = "SORT";


export interface SortAction extends Action<typeof SORT> {
	sortField: keyof ArrivalItem,
	sortType: SortType
}

export type DataActions = SortAction;


export const sort = (sortField: keyof ArrivalItem, sortType: SortType): SortAction => ({
	type: SORT,
	sortField,
	sortType
});