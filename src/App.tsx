import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect, MapStateToProps } from 'react-redux'
import CurrencySelect from './containers/CurrencySelect';
import { State } from './reducers';
import ArrivalDataView from './components/ArrivalDataView';


interface AppProps {
	hasErrors: boolean,
	errorText: string | null
}

type ComponentProps = AppProps;

class App extends React.Component<ComponentProps> {

	renderCurrencyErr() {
		let { hasErrors, errorText } = this.props;
		if (hasErrors) {
			return <div className="col-12 text-danger text-right">{errorText}</div>
		}
		return null;
	}

	render() {
		return (
			<div className="container py-3">
				<div className="form-row">
					<div className="col-auto ml-auto">
						<CurrencySelect />
					</div>
					{this.renderCurrencyErr()}
				</div>
				<ArrivalDataView />
			</div>
		);
	}
}

const mapStateToProps: MapStateToProps<AppProps, {}, State> = (state: State) => ({
	hasErrors: state.currency.hasErrors,
	errorText: state.currency.errorText
});

export default connect(mapStateToProps)(App);
