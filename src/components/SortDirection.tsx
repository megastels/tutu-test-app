import { SortType } from "../actions";
import React from 'react';
import { faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export interface SortDirectionProps {
	type: SortType;
}

const SortDirection: React.FC<SortDirectionProps> = (props: SortDirectionProps) => {
	let { type } = props;
	switch (type) {
		case SortType.ASC:
			return <FontAwesomeIcon icon={faSortUp} />
		case SortType.DESC:
			return <FontAwesomeIcon icon={faSortDown} />
		case SortType.NO:
			return <FontAwesomeIcon icon={faSort} />
	}
}

export default SortDirection;