import React, { Dispatch } from "react";
import { ArrivalItem } from "../interfaces/ArrivalItem";
import { CurrencyItem } from "../interfaces/CurrencyItem";
import SortDirection from "./SortDirection";
import { SortType, sort, SortAction } from "../actions";
import { State } from "../reducers";
import { MapStateToProps, connect, MapDispatchToProps } from "react-redux";

export interface ArrivalDataViewProps {
	currency: CurrencyItem,
	data: ArrivalItem[],
	sortKey: keyof ArrivalItem,
	sortType: SortType
}

export interface ArrivalDataViewDispatchProps {
	onSortChange: (key: keyof ArrivalItem, type: SortType) => void;
}

type ComponentProps = ArrivalDataViewProps & ArrivalDataViewDispatchProps;

class ArrivalDataView extends React.Component<ComponentProps>{

	readonly intl = new Intl.DateTimeFormat("ru-RU", { hour12: false, hour: "numeric", minute: "numeric" });

	_changePriceSort = (e: React.MouseEvent<HTMLButtonElement>) => this.changePriceSort(e);
	changePriceSort(e: React.MouseEvent<HTMLButtonElement>) {
		e.preventDefault();

		let nextSort: SortType;
		switch (this.props.sortType) {
			case SortType.NO:
				nextSort = SortType.ASC;
				break;
			case SortType.ASC:
				nextSort = SortType.DESC;
				break;
			default:
				nextSort = SortType.NO;
		}
		this.props.onSortChange("price", nextSort);
	}

	formatDate(date: Date) {
		return this.intl.format(date);
	}

	formatPrice(realPrice: number) {
		let currency = this.props.currency;
		let price = Math.round(realPrice * 100 / currency.nominal) / 100;

		return `${price} ${currency.value}`
	}

	render() {
		let { data, sortType } = this.props;

		return (
			<table className="mt-2 table">
				<thead>
					<tr>
						<th>Отправление</th>
						<th>Прибытие</th>
						<th colSpan={2}>
							<button className="btn btn-link p-0 pr-2" onClick={this._changePriceSort}>Цена</button>
							<SortDirection type={sortType} />
						</th>
					</tr>
				</thead>
				<tbody>
					{data.map(item => (
						<tr key={item.index}>
							<td>
								<b>{this.formatDate(item.arrivalDate)}</b><br />
								{item.arrival}
							</td>
							<td>
								<b>{this.formatDate(item.departureDate)}</b><br />
								{item.departure}
							</td>
							<td>{this.formatPrice(item.price)}</td>
							<td>
								<button className="btn btn-dark">Купить</button>
							</td>
						</tr>)
					)}
				</tbody>
			</table>
		);
	}
}

const mapStateToProps: MapStateToProps<ArrivalDataViewProps, {}, State> = (state: State) => ({
	currency: state.currency.selectedCurrency,
	data: state.data.list,
	sortKey: state.data.sortKey,
	sortType: state.data.sortType
});

const mapDispatchToProps: MapDispatchToProps<ArrivalDataViewDispatchProps, {}> = (dispatch: Dispatch<SortAction>) => ({
	onSortChange: (key, type) => dispatch(sort(key, type))
})

export default connect(mapStateToProps, mapDispatchToProps)(ArrivalDataView);