import React, { ChangeEvent } from 'react';

export interface SelectItem {
	name: string;
	value: string;
}

export interface SelectProps<T extends SelectItem> {
	selected?: T,
	values: T[],
	disabled: boolean
}
export interface SelectDispatchProps<T extends SelectItem> {
	onChange: (item: T) => void
}

type ComponentProps<T extends SelectItem> = SelectProps<T> & SelectDispatchProps<T>


class Select<T extends SelectItem> extends React.Component<ComponentProps<T>> {

	_change = (e: ChangeEvent<HTMLSelectElement>) => this.change(e);
	change(e: ChangeEvent<HTMLSelectElement>) {
		e.preventDefault();
		let item = this.props.values.find(item => item.value === e.target.value)
		if (item)
			this.props.onChange(item);
	}

	render() {
		const { values } = this.props;
		let selected = this.props.selected ? this.props.selected.value : undefined;
		return (
			<select className="custom-select" value={selected} onChange={this._change} disabled={this.props.disabled}>
				<option hidden value=""></option>
				{values.map(item => <option key={item.value} value={item.value}>{item.name}</option>)}
			</select>
		);
	}

}

export default Select;