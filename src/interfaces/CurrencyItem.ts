import { SelectItem } from "../components/Select";

export interface CurrencyItem extends SelectItem {
	nominal: number;
}