export interface ArrivalItem {
	id: number,
	index: number,
	departure: string,
	arrival: string,
	departureDate: Date,
	arrivalDate: Date,
	price: number
}