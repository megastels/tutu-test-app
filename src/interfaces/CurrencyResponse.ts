export interface CurrencyResponse {
	Date: string,
	PreviousDate: string,
	PreviousURL: string,
	Timestamp: string,
	Valute: ValuteList
}

export interface ValuteList {
	[code: string]: Valute
}

export interface Valute {
	ID: string,
	NumCode: string,
	CharCode: string,
	Nominal: number,
	Name: string,
	Value: number,
	Previous: number
}