import { ThunkAction } from "redux-thunk";
import { AnyAction } from "redux";
import { State } from "../reducers";

export type PseudoAction<R> = ThunkAction<R, State, undefined, AnyAction>;